/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package book;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


class SearchFrame extends JFrame{
    JPanel searchPanel;
    JTextField searchText;
    JLabel searchLabel;
    JTable viewTable2;
        DefaultTableModel tableModel2;
   
    
    public SearchFrame(){
        viewTable2=new JTable();
        
           tableModel2= new DefaultTableModel(new Object[0][6], new String[]{
            "Id", "Book Title", "Author", "Amount", "Publication Date", "Price", "Country"});
        
        
        viewTable2.setModel(tableModel2);
        
   searchText=new JTextField(20);
   searchLabel=new JLabel("Search");
   searchPanel=new JPanel(new FlowLayout());
   searchPanel.add(searchLabel);
   searchPanel.add(searchText);
   
   
   add(searchPanel,BorderLayout.NORTH);
         add(new JScrollPane(viewTable2),BorderLayout.CENTER);

    }
    
    public static void main(String[] args){
         SearchFrame frame1=new SearchFrame();
        frame1.setSize(500, 700);
        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame1.setLocationRelativeTo(null);
        frame1.setVisible(true);
    
    
    
    
    }

  
}
