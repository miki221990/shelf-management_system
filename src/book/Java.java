package book;

import java.awt.*;
import java.sql.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Java extends JFrame implements ActionListener {



    public JLabel title;
    public JLabel author;
    public JLabel publication;
    //public JLabel booktitle;
    public JLabel amount;
    public JLabel pubDate;
    public JLabel price;
    public JLabel country;
    public JTextField titleText;
    public JTextField authorText;
    //public JTextField publicationText;
    //public JTextField booktitleText;
    public JTextField amountText;
    public JTextField pubDateText;
    public JTextField priceText;
    public JComboBox chooseCountry;

    public JButton register;
    public JButton update;
    public JButton delete;
    public JButton retrive;
    public JButton search;
    public JButton save;

    public JPanel form1;
    public JPanel buttonsHolder;
    public JPanel buttons;
    public JPanel savePanel;

    public JLabel titleValidation;
    public JLabel authorValidation;
    public JLabel amountValidation;
    public JLabel dateValidation;
    public JLabel priceValidation;
    
    
   
    public JTable viewTable;
    DefaultTableModel tableModel;
    String[] clist = {"Ethiopia", "USA", "Uganda", "Sudan", "UAE", "Britain"};
    Connection connection = null;
    Statement statement = null;
    ResultSet resultSet = null;
    int counter;
    public Java() {
        super("Book Managment");
        counter=23;
        tableModel = new DefaultTableModel(new Object[0][6], new String[]{
            "Id", "Book Title", "Author", "Amount", "Publication Date", "Price", "Country"});
        viewTable = new JTable();
        viewTable.setModel(tableModel);
        titleValidation = new JLabel("");
        authorValidation = new JLabel("");
        amountValidation = new JLabel("");
        dateValidation = new JLabel("");
        priceValidation = new JLabel("");
        //Color clor=new Color(148,50,190);
        Color clor2=new Color(250,200,150);
        Color clor=new Color(240,140,100);
        

        titleValidation.setForeground(Color.red);
        authorValidation.setForeground(Color.red);
        amountValidation.setForeground(Color.red);
        dateValidation.setForeground(Color.red);
        priceValidation.setForeground(Color.red);

        titleValidation.setPreferredSize(new Dimension(200, 20));
        authorValidation.setPreferredSize(new Dimension(200, 20));
        amountValidation.setPreferredSize(new Dimension(200, 20));
        dateValidation.setPreferredSize(new Dimension(200, 20));
        priceValidation.setPreferredSize(new Dimension(200, 20));

        GridLayout flow = new GridLayout(7,15);
        //flow.setAlignment(FlowLayout.LEFT);
       // form1.setBounds(10,10, 500, 700);
        form1 = new JPanel(flow);
        
        title = new JLabel("Title: ");
        titleText = new JTextField(20);
        author = new JLabel("Author: ");
        authorText = new JTextField(20);
        amount = new JLabel("Amount: ");
        amountText = new JTextField(20);
        pubDate = new JLabel("YearOfPublication: ");
        pubDateText = new JTextField(20);
        price = new JLabel("Price: ");
       
       
        priceText = new JTextField(20);
        country = new JLabel("Country: ");
        chooseCountry = new JComboBox(clist);

        buttonsHolder = new JPanel(new BorderLayout());
        buttonsHolder.setPreferredSize(new Dimension(20, 100));
        form1.add(title);
        form1.add(titleText);
        form1.add(titleValidation);
        form1.add(author);
        form1.add(authorText);
        form1.add(authorValidation);

        JPanel buttonAndForm = new JPanel(new BorderLayout());
        form1.add(amount);
        form1.add(amountText);
        form1.add(amountValidation);
        form1.add(pubDate);
        form1.add(pubDateText);
        form1.add(dateValidation);
        form1.add(price);
        
        form1.add(priceText);
        form1.add(priceValidation);
        form1.add(country);
        form1.add(chooseCountry);
        form1.setBackground(clor);

//        textField event handlers
        authorText.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(20);
                        } catch (InterruptedException ex) {
                        }
                        char key;
                        boolean flag = false;
                        String string = authorText.getText();
                        for (int i = 0; i < string.length(); i++) {
                            key = string.charAt(i);
                            if (!(Character.isLetter(key) || Character.isISOControl(key) || key == ' ')) {
                                flag = true;
                                break;
                            }
                        }
                        if (flag) {
                            authorValidation.setText("Invalid Entry");
                        } else {
                            authorValidation.setText("");
                        }
                    }
                });
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }

        });

        pubDateText.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(20);
                        } catch (InterruptedException ex) {
                        }
                        char key;
                        boolean flag = false;
                        String string = pubDateText.getText();
                        for (int i = 0; i < string.length(); i++) {
                            if (string.length() > 4) {
                                flag = true;
                                break;
                            }
                            key = string.charAt(i);
                            if (!(Character.isDigit(key) || Character.isISOControl(key))) {
                                flag = true;
                                break;
                            }
                        }
                        if (flag) {
                            dateValidation.setText("Invalid Entry");
                        } else {
                            dateValidation.setText("");
                        }
                    }
                });

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }

        });

        authorText.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                char key = e.getKeyChar();

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }

        });

        authorText.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                char key = e.getKeyChar();

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }

        });

        viewTable.setSelectionMode(0);
        buttons = new JPanel(new FlowLayout());

        savePanel = new JPanel(new FlowLayout());
        save = new JButton("save");
        register = new JButton("Register");
        update = new JButton("Update");
        delete = new JButton("Delete");
        retrive = new JButton("Retrive");
        search = new JButton("Search");

//        registering buttons to action listeners
        register.addActionListener(this);
        update.addActionListener(this);
        delete.addActionListener(this);
        search.addActionListener(this);
        retrive.addActionListener(this);

        savePanel.add(register, BorderLayout.CENTER);
//        buttons.add(register);
        buttons.add(update);
        buttons.add(delete);
        buttons.add(retrive);
        buttons.add(search);

        buttonsHolder.add(savePanel, BorderLayout.NORTH);
        buttonsHolder.add(buttons, BorderLayout.CENTER);

        buttonAndForm.add(form1, BorderLayout.CENTER);
        buttonAndForm.add(buttonsHolder, BorderLayout.SOUTH);
        add(buttonAndForm, BorderLayout.CENTER);
        JPanel p9=new JPanel();
        p9.setBackground(new Color(67,137,140));
        p9.add(new JScrollPane(viewTable));
        add(p9, BorderLayout.EAST);
        
        //add(new JScrollPane(viewTable), BorderLayout.EAST);
        viewTable.setBackground(clor2);

    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                Java frame = new Java();
                frame.setSize(500, 600);
                frame.setLocationRelativeTo(null);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == register) {

            if (validateDataEntry()) {
                String[] data = new String[7];
                data[0] = Integer.toString(counter);
                data[1] = titleText.getText();
                data[2] = authorText.getText();
                data[3] = amountText.getText();
                data[4] = pubDateText.getText();
                data[5] = priceText.getText();
                data[6] = chooseCountry.getSelectedItem().toString();

                int amount = Integer.parseInt(data[3]);
                float price = Float.parseFloat(data[5]);
                tableModel.addRow(data);

                try {
              
                    connection = DriverManager.getConnection("jdbc:mysql://localhost/AASTULibrary", "root", "mikiyas123");
                    statement = connection.createStatement();
                    int x = statement.executeUpdate("insert into BookList values("+0+",\"" +data[1] + "\",\"" + data[2]
                            +  "\",\"" + data[4]+"-01-01\"," + amount + ","+ price + ",\"" + data[6] + "\")");
                    counter++;
                } catch (SQLException ex) {
                    ex.printStackTrace();

                } finally {
                    try {
                        
                        statement.close();
                        connection.close();
                    } catch (SQLException v) {
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "There is something wrong with the data entry!");
            }
        } else if (e.getSource() == update) {
            int x = viewTable.getSelectedRow();
            System.out.println(x);
            if (x == -1) {
                JOptionPane.showMessageDialog(null, "Please select a row!");
                return;
            }
            int y;
            y = JOptionPane.showConfirmDialog(null, "are you sure, do you want to update this row?");
            if (validateDataEntry()) {
                if (y == 0) {
                    String[] data = new String[7];
                    data[0] = viewTable.getValueAt(x, 0).toString();
                    data[1] = titleText.getText();
                    data[2] = authorText.getText();
                    data[3] = amountText.getText();
                    data[4] = pubDateText.getText();
                    data[5] = priceText.getText();
                    data[6] = chooseCountry.getSelectedItem().toString();
                    tableModel.removeRow(x);
                    tableModel.insertRow(x, data);
                    int amount = Integer.parseInt(data[3]);
                    float price = Float.parseFloat(data[5]);

                    try {
                        connection = DriverManager.getConnection("jdbc:mysql://localhost/AASTULibrary", "root", "mikiyas123");
                        statement = connection.createStatement();
                        int ret;
                        ret = statement.executeUpdate("update BookList set Booktitle=\""+data[1]+"\", Author=\""+data[2]+"\""
                                + ", PublicationDate=\""+data[4]+"-01-01\", Amount="+amount+", Price="+price+
                                ", Country= \""+data[6]+"\" where id="+Integer.parseInt(viewTable.getValueAt(x, 0).toString()));
                    } catch (SQLException ex) {

                    } finally {
                        try {
                            
                            statement.close();
                            connection.close();
                        } catch (SQLException v) {
                        }
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "There is something wrong with the data entry!");
            }
        } else if (e.getSource() == delete) {
            int x = viewTable.getSelectedRow();
            System.out.println(x);
            if (x == -1) {
                JOptionPane.showMessageDialog(null, "Please select a row!");
                return;
            }
            int y;
            y = JOptionPane.showConfirmDialog(null, "are you sure, do you want to delete this row?");
            if (y == 0) {
                tableModel.removeRow(x);
                try {
                    connection = DriverManager.getConnection("jdbc:mysql://localhost/AASTULibrary", "root", "mikiyas123");
                    statement = connection.createStatement();
                    int ret;
                    ret = statement.executeUpdate("delete from BookList where id= " + x);
                } catch (SQLException ex) {

                } finally {
                    try {
                        
                        statement.close();
                        connection.close();
                    } catch (SQLException v) {
                    }
                }
            }

        } else if (e.getSource() == retrive) {
            int y = viewTable.getSelectedRow();
            JOptionPane.showMessageDialog(null, "Id: " + viewTable.getValueAt(y, 0)
                    + "\nTitle: " + viewTable.getValueAt(y, 1) + "\nAuthor: " + viewTable.getValueAt(y, 2)
                    + "\nAmount: " + viewTable.getValueAt(y, 3) + "\nYear Of Publication: " + viewTable.getValueAt(y, 4)
                    + "\nPrice: " + viewTable.getValueAt(y, 5) + "\nCountry: " + viewTable.getValueAt(y, 6));
        } else if (e.getSource() == search) {
        SearchFrame frame3=new SearchFrame();
     
          
           
           
            
            
            
//            int x;
//            x=Integer.parseInt(JOptionPane.showInputDialog("input the id of the book"));
//              int y = viewTable.getRowCount();
//            for (int i=0;i<y;i++){
//                int w=viewTable.getSelectedColumnCount();
//                if (w==0){
//                    int q=Integer.parseInt((String) viewTable.getValueAt(i, w));
//                    if (x==q){
//                          JOptionPane.showMessageDialog(null, "Id: " + viewTable.getValueAt(i, 0)
//                    + "\nTitle: " + viewTable.getValueAt(i, 1) + "\nAuthor: " + viewTable.getValueAt(i, 2)
//                    + "\nAmount: " + viewTable.getValueAt(i, 3) + "\nYear Of Publication: " + viewTable.getValueAt(i, 4)
//                    + "\nPrice: " + viewTable.getValueAt(i, 5) + "\nCountry: " + viewTable.getValueAt(i, 6));
//                    
                    
                    
                    }
                }

    private boolean validateDataEntry() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
                    
           
            
            
            }
            
            
            //int z=viewTable.getSelectedRow();
            
            

        
    

//    public boolean validateDataEntry() {
//        if (authorValidation.getText().equals("") && amountValidation.getText().equals("")
//                && dateValidation.getText().equals("") && priceValidation.getText().equals("")) {
//            return true;
//        }
//        return false;
//    }
//

